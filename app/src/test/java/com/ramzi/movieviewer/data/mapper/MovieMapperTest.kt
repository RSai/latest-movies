package com.ramzi.movieviewer.data.mapper

import com.ramzi.movieviewer.data.model.DiscoverMovieListApi
import com.ramzi.movieviewer.data.model.GenreApi
import com.ramzi.movieviewer.data.model.MovieApi
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieMapperTest {

    lateinit var movieMapper: MovieMapper

    companion object {
        const val PAGE = 1
        const val ID = 1L
        const val TITLE = "title"
        const val RELEASE_DATE = "releaseDate"
        const val GENRE_1 = "genre1"
        const val GENRE_2 = "genre2"
        const val OVERVIEW = "overview"
        const val LANGUAGE = "language"
        const val TAG_LINE = "tagLine"
        const val POSTER_PATH = "posterPath"
    }

    @Before
    fun setUp() {
        movieMapper = MovieMapper()
    }

    @Test
    fun `should map DiscoverMovieListApi to MovieModel List`() {
        // given
        var movieList: MutableList<MovieApi> = mutableListOf(MovieApi(id = ID, title = TITLE, releaseDate = RELEASE_DATE, genres = null))
        var discoverMovieListApi: DiscoverMovieListApi = DiscoverMovieListApi(PAGE, movieList)

        // when
        val out = movieMapper.transform(discoverMovieListApi)

        // then
        Assert.assertEquals("List size should be 1", 1, out.size)
        Assert.assertEquals(ID, out[0].id)
        Assert.assertEquals(TITLE, out[0].title)
        Assert.assertEquals(RELEASE_DATE, out[0].releaseDate)
        Assert.assertEquals(null, out[0].genres)
    }

    @Test
    fun `should map MovieApi to MovieModel`() {
        // given
        var genres = mutableListOf(GenreApi(ID, GENRE_1), GenreApi(ID, GENRE_2))
        var movieApi = MovieApi(
                id = ID,
                title = TITLE,
                releaseDate = RELEASE_DATE,
                genres = genres,
                overview = OVERVIEW,
                originalLanguage = LANGUAGE,
                tagLine = TAG_LINE,
                posterPath = POSTER_PATH)

        // when
        val out = movieMapper.transform(movieApi)

        // then
        Assert.assertEquals(ID, out.id)
        Assert.assertEquals(TITLE, out.title)
        Assert.assertEquals(RELEASE_DATE, out.releaseDate)
        Assert.assertEquals("$GENRE_1, $GENRE_2", out.genres)
        Assert.assertEquals(OVERVIEW, out.overview)
        Assert.assertEquals(LANGUAGE, out.originalLanguage)
        Assert.assertEquals(TAG_LINE, out.tagLine)
        Assert.assertEquals(POSTER_PATH, out.posterPath)
    }
}