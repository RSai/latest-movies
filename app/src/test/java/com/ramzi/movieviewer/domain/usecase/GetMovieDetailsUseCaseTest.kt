package com.ramzi.movieviewer.domain.usecase

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.ramzi.movieviewer.data.repository.MovieRepositoryImpl
import com.ramzi.movieviewer.domain.executor.PostExecutionThread
import com.ramzi.movieviewer.domain.executor.ThreadExecutor
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.domain.rx.RxFactory
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetMovieDetailsUseCaseTest {

    companion object {
        const val ID = 1L
    }

    lateinit var getMovieDetailsUseCase: GetMovieDetailsUseCase

    @Mock
    lateinit var mockMovieRepositoryImpl: MovieRepositoryImpl

    @Mock
    lateinit var mockthreadExecutor: ThreadExecutor

    @Mock
    lateinit var mockpostExecutionThread: PostExecutionThread

    @Mock
    lateinit var mockrxFactory: RxFactory

    @Mock
    lateinit var mockSingleMovieModel: Single<MovieModel>

    @Before
    fun setUp() {
        whenever(mockrxFactory.getSchedulerFromExecutor(mockthreadExecutor)).thenReturn(Schedulers.from(mockthreadExecutor))
        whenever(mockpostExecutionThread.scheduler).thenReturn(AndroidSchedulers.mainThread())
        getMovieDetailsUseCase = GetMovieDetailsUseCase(mockthreadExecutor, mockpostExecutionThread, mockrxFactory, mockMovieRepositoryImpl)
    }

    @Test
    fun `should call getMovieDetails() when use case is executed`() {
        // given
        whenever(mockMovieRepositoryImpl.getMovieDetails(ID)).thenReturn(mockSingleMovieModel)

        // when
        val testObserver = getMovieDetailsUseCase.buildUseCaseSingle(ID).test()

        verify(mockMovieRepositoryImpl).getMovieDetails(ID)
    }
}