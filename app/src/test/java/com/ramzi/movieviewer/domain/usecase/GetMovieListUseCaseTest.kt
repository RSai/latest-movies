package com.ramzi.movieviewer.domain.usecase

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.ramzi.movieviewer.data.repository.MovieRepositoryImpl
import com.ramzi.movieviewer.domain.executor.PostExecutionThread
import com.ramzi.movieviewer.domain.executor.ThreadExecutor
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.domain.rx.RxFactory
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetMovieListUseCaseTest {

    companion object {
        const val PAGE = 1
    }

    lateinit var getMovieDetailsUseCase: GetMovieListUseCase

    @Mock
    lateinit var mockMovieRepositoryImpl: MovieRepositoryImpl

    @Mock
    lateinit var mockthreadExecutor: ThreadExecutor

    @Mock
    lateinit var mockpostExecutionThread: PostExecutionThread

    @Mock
    lateinit var mockrxFactory: RxFactory

    @Mock
    lateinit var mockSingleListMovieModel: Single<List<MovieModel>>

    @Before
    fun setUp() {
        whenever(mockrxFactory.getSchedulerFromExecutor(mockthreadExecutor)).thenReturn(Schedulers.from(mockthreadExecutor))
        whenever(mockpostExecutionThread.scheduler).thenReturn(AndroidSchedulers.mainThread())
        getMovieDetailsUseCase = GetMovieListUseCase(mockthreadExecutor, mockpostExecutionThread, mockrxFactory, mockMovieRepositoryImpl)
    }

    @Test
    fun `should call getMovieDetails() when use case is executed`() {
        // given
        whenever(mockMovieRepositoryImpl.getLatestMovies(PAGE)).thenReturn(mockSingleListMovieModel)

        // when
        val testObserver = getMovieDetailsUseCase.buildUseCaseSingle(PAGE).test()

        verify(mockMovieRepositoryImpl).getLatestMovies(PAGE)
    }
}