package com.ramzi.movieviewer.presentation.di.module

import com.ramzi.movieviewer.data.mapper.MovieMapper
import com.ramzi.movieviewer.data.network.RequestFactory
import com.ramzi.movieviewer.data.repository.MovieRepositoryImpl
import com.ramzi.movieviewer.domain.repository.MovieRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun provideMovieRepository(requestFactory: RequestFactory,
                               movieMapper: MovieMapper)
            : MovieRepository = MovieRepositoryImpl(requestFactory, movieMapper)
}