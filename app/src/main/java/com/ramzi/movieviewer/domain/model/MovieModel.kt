package com.ramzi.movieviewer.domain.model

data class MovieModel(
        val id: Long,
        val title: String? = null,
        val overview: String? = null,
        val releaseDate: String? = null,
        val originalLanguage: String? = null,
        val genres: String? = null,
        val tagLine: String? = null,
        val posterPath: String? = null
)