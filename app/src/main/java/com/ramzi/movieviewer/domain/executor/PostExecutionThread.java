package com.ramzi.movieviewer.domain.executor;

import io.reactivex.Scheduler;

/**
 * This interface represents the main thread provider
 * for the result of {@link com.ramzi.movieviewer.domain.usecase.base.BaseReactiveUseCase}.
 */
public interface PostExecutionThread {

    /**
     * This method returns main thread (UI) scheduler.
     *
     * @return a ui notification thread
     */
    Scheduler getScheduler();
}
