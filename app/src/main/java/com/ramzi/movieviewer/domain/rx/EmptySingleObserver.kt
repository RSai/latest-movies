package com.ramzi.movieviewer.domain.rx

import io.reactivex.observers.DisposableSingleObserver

/**
 * This is the default handler for business use cases.
 */
open class EmptySingleObserver<T> : DisposableSingleObserver<T>() {

    override fun onSuccess(result: T) {
    }

    override fun onError(throwable: Throwable) {
    }
}
