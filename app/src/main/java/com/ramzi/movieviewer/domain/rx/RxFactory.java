package com.ramzi.movieviewer.domain.rx;


import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Factory for returning Rx Schedulers used by {@link com.ramzi.movieviewer.domain.usecase.base.BaseReactiveUseCase}
 */
@Singleton
public class RxFactory {

    @NonNull
    public Scheduler getIoScheduler() {
        return Schedulers.io();
    }

    @NonNull
    public Scheduler getMainThreadScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @NonNull
    public Scheduler getSchedulerFromExecutor(@NonNull Executor executor) {
        return Schedulers.from(executor);
    }
}
