package com.ramzi.movieviewer.domain.executor;

import java.util.concurrent.Executor;

/**
 * This interface represents the background task executor which
 * is used to perform operations defined by {@link com.ramzi.movieviewer.domain.usecase.base.BaseReactiveUseCase} classes.
 */
public interface ThreadExecutor extends Executor {
}
