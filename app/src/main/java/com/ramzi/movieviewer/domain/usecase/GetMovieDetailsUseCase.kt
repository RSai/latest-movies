package com.ramzi.movieviewer.domain.usecase

import com.ramzi.movieviewer.domain.executor.PostExecutionThread
import com.ramzi.movieviewer.domain.executor.ThreadExecutor
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.domain.repository.MovieRepository
import com.ramzi.movieviewer.domain.rx.RxFactory
import com.ramzi.movieviewer.domain.usecase.base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetMovieDetailsUseCase
@Inject constructor(threadExecutor: ThreadExecutor,
                    postExecutionThread: PostExecutionThread,
                    rxFactory: RxFactory,
                    private val movieRepository: MovieRepository)
    : SingleUseCase<MovieModel, Long>(threadExecutor, postExecutionThread, rxFactory) {

    override fun buildUseCaseSingle(params: Long?): Single<MovieModel> {
        return movieRepository.getMovieDetails(params)
    }
}