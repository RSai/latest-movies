package com.ramzi.movieviewer.domain.di.module

import com.ramzi.movieviewer.data.executor.ExecutorFactory
import com.ramzi.movieviewer.data.executor.TaskExecutor
import com.ramzi.movieviewer.domain.executor.PostExecutionThread
import com.ramzi.movieviewer.domain.executor.ThreadExecutor
import com.ramzi.movieviewer.domain.rx.RxFactory
import com.ramzi.movieviewer.presentation.UIThread
import dagger.Module
import dagger.Provides

@Module
class ExecutorModule {
    @Provides
    fun providePostExecutionThread(): PostExecutionThread = UIThread()

    @Provides
    fun provideThreadExecutor(executorFactory: ExecutorFactory): ThreadExecutor = TaskExecutor(executorFactory)

    @Provides
    fun provideRxFactory(): RxFactory = RxFactory()
}