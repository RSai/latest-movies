package com.ramzi.movieviewer.domain.repository

import com.ramzi.movieviewer.domain.model.MovieModel
import io.reactivex.Single

interface MovieRepository {

    fun getLatestMovies(page: Int?): Single<List<MovieModel>>
    fun getMovieDetails(id: Long?): Single<MovieModel>
}