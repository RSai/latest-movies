package com.ramzi.movieviewer.domain.di.component

import com.ramzi.movieviewer.domain.di.module.ExecutorModule
import com.ramzi.movieviewer.domain.repository.MovieRepository
import com.ramzi.movieviewer.domain.usecase.GetMovieDetailsUseCase
import com.ramzi.movieviewer.domain.usecase.GetMovieListUseCase
import com.ramzi.movieviewer.presentation.di.module.RepositoryModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ExecutorModule::class, RepositoryModule::class])
interface DomainComponent {

    fun provideGetMovieListUseCase(): GetMovieListUseCase

    fun provideGetMovieDetailsUseCase(): GetMovieDetailsUseCase

    fun provideMovieRepository(): MovieRepository

}