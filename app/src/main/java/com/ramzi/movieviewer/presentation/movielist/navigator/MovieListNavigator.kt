package com.ramzi.movieviewer.presentation.movielist.navigator

import android.content.Context
import com.ramzi.movieviewer.presentation.moviedetails.view.MovieDetailsActivity
import javax.inject.Inject

/**
 * This class is used to navigate the user from {@link com.ramzi.movieviewer.presentation.movielist.view.MovieListActivity}
 */
class MovieListNavigator
@Inject constructor() {
    fun navigateToMovieDetails(context: Context, id: Long?) {
        id?.run {
            context.startActivity(MovieDetailsActivity.getStartIntent(context, id))
        }
    }
}