package com.ramzi.movieviewer.presentation.di.component

import com.ramzi.movieviewer.domain.di.module.ExecutorModule
import com.ramzi.movieviewer.presentation.di.module.PresenterModule
import com.ramzi.movieviewer.presentation.di.module.RepositoryModule
import com.ramzi.movieviewer.presentation.moviedetails.presenter.MovieDetailsPresenter
import com.ramzi.movieviewer.presentation.moviedetails.view.MovieDetailsActivity
import com.ramzi.movieviewer.presentation.movielist.presenter.MovieListPresenter
import com.ramzi.movieviewer.presentation.movielist.view.MovieListActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [PresenterModule::class, ExecutorModule::class, RepositoryModule::class])
interface PresentationComponent {

    fun provideMovieListPresenter(): MovieListPresenter

    fun provideMovieDetailsPresenter(): MovieDetailsPresenter

    fun inject(movieListActivity: MovieListActivity)

    fun inject(movieDetailsActivity: MovieDetailsActivity)
}