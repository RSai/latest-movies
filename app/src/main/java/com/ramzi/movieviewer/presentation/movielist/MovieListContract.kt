package com.ramzi.movieviewer.presentation.movielist

import android.support.annotation.StringRes
import android.support.annotation.UiThread
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.presentation.MvpView
import com.ramzi.movieviewer.presentation.PresenterInterface

/**
 * Interfaces for Movie list view and presenter.
 */
interface MovieListContract {

    interface View : MvpView {

        @UiThread
        fun addToMovieList(movieList: List<MovieModel>)

        @UiThread
        fun showError(@StringRes errorMessage: Int)

        @UiThread
        fun showProgressBar()

        @UiThread
        fun hideProgressBar()
    }

    interface Presenter : PresenterInterface<View> {

        fun fetchMovieListFirstPage()

        fun fetchMovieListNextPage()
    }
}
