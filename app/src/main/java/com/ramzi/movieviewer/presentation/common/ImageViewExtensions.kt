package com.ramzi.movieviewer.presentation.common

import android.widget.ImageView
import com.ramzi.movieviewer.R
import com.squareup.picasso.Picasso


fun ImageView.loadNetworkImage(path: String) {
    Picasso.get().load(path)
            .placeholder(R.drawable.poster_placeholder)
            .into(this)
}
