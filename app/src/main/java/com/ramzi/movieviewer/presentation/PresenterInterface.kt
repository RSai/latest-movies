package com.ramzi.movieviewer.presentation

import android.support.annotation.CallSuper

/**
 * Every presenter should extend this class
 */
interface PresenterInterface<VIEW : MvpView> {

    /**
     * Should be called when the activity or fragment is visible to the user
     */
    @CallSuper
    fun attachView(mvpView: VIEW)

    /**
     * * Should be called when the activity or fragment is not visible to the user anymore
     */
    @CallSuper
    fun detachView()

}

