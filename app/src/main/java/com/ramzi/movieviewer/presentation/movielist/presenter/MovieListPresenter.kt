package com.ramzi.movieviewer.presentation.movielist.presenter

import android.util.Log
import com.ramzi.movieviewer.R
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.domain.usecase.GetMovieListUseCase
import com.ramzi.movieviewer.presentation.BasePresenter
import com.ramzi.movieviewer.presentation.movielist.MovieListContract
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject
import javax.inject.Singleton

/**
 * This is the presenter for the Movie List activity
 */
@Singleton
class MovieListPresenter
@Inject constructor(private val getMovieListUseCase: GetMovieListUseCase) : BasePresenter<MovieListContract.View>(), MovieListContract.Presenter {

    private var pageNumber: Int? = 1

    override fun fetchMovieListFirstPage() {
        pageNumber = 1
        getMovieListUseCase.execute(MovieListObserver(), pageNumber)
        mvpView?.showProgressBar()
    }

    override fun fetchMovieListNextPage() {
        pageNumber = pageNumber?.inc()
        getMovieListUseCase.execute(MovieListObserver(), pageNumber)
        mvpView?.showProgressBar()
    }

    inner class MovieListObserver : DisposableSingleObserver<List<MovieModel>>() {
        override fun onSuccess(movieList: List<MovieModel>) {
            mvpView?.addToMovieList(movieList)
            mvpView?.hideProgressBar()
        }

        override fun onError(e: Throwable) {
            Log.e(MovieListPresenter::class.simpleName, e.toString())
            mvpView?.showError(R.string.network_error_message)
            mvpView?.hideProgressBar()
        }

    }
}
