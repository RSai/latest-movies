package com.ramzi.movieviewer.presentation.movielist.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.ramzi.movieviewer.domain.model.MovieModel

class MovieListAdapter(private var onClickListener: View.OnClickListener) : RecyclerView.Adapter<MovieViewHolder>() {

    private var movieList: MutableList<MovieModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder.newInstance(parent)
    }

    override fun getItemCount(): Int = movieList.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.apply {
            title.text = movieList[position].title
            releaseDate.text = movieList[position].releaseDate
            rootLayout.tag = movieList[position].id
            rootLayout.setOnClickListener(onClickListener)
        }
    }

    fun addToMovieList(movieList: List<MovieModel>) {
        this.movieList.addAll(movieList)
        notifyDataSetChanged()
    }

}
