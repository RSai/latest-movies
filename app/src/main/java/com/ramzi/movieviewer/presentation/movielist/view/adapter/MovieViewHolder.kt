package com.ramzi.movieviewer.presentation.movielist.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.ramzi.movieviewer.R

class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    companion object {
        @JvmStatic
        fun newInstance(parent: ViewGroup): MovieViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                    R.layout.movie_list_item,
                    parent,
                    false
            )
            return MovieViewHolder(view)
        }
    }

    @BindView(R.id.movie_list_item_root_layout)
    lateinit var rootLayout: LinearLayout

    @BindView(R.id.movie_list_item_title)
    lateinit var title: TextView

    @BindView(R.id.movie_list_item_release_date)
    lateinit var releaseDate: TextView


    init {
        ButterKnife.bind(this, itemView)
    }
}
