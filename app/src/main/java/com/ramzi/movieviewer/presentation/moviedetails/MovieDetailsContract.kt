package com.ramzi.movieviewer.presentation.moviedetails

import android.support.annotation.StringRes
import android.support.annotation.UiThread
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.presentation.MvpView
import com.ramzi.movieviewer.presentation.PresenterInterface

/**
 * Interfaces for Movie details view and presenter.
 */
interface MovieDetailsContract {

    interface View : MvpView {

        @UiThread
        fun showMovieDetails(movieModel: MovieModel)

        @UiThread
        fun showError(@StringRes errorMessage: Int)

        @UiThread
        fun showProgressBar()

        @UiThread
        fun hideProgressBar()
    }

    interface Presenter : PresenterInterface<View> {

        fun fetchMovieDetails(id: Long)
    }
}
