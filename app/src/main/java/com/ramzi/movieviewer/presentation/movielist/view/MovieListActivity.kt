package com.ramzi.movieviewer.presentation.movielist.view

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import com.ramzi.movieviewer.R
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.presentation.di.component.DaggerPresentationComponent
import com.ramzi.movieviewer.presentation.di.component.PresentationComponent
import com.ramzi.movieviewer.presentation.movielist.MovieListContract
import com.ramzi.movieviewer.presentation.movielist.navigator.MovieListNavigator
import com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieListAdapter
import javax.inject.Inject

class MovieListActivity : AppCompatActivity(), MovieListContract.View, View.OnClickListener {

    @Inject
    lateinit var presenter: MovieListContract.Presenter

    @Inject
    lateinit var movieListNavigator: MovieListNavigator

    @BindView(R.id.movie_list_activity_root_layout)
    lateinit var rootLayout: FrameLayout

    @BindView(R.id.movie_list_activity_recycler_view)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.movie_list_activity_progress_bar)
    lateinit var progressBar: ProgressBar

    val presentationComponent: PresentationComponent by lazy {
        DaggerPresentationComponent.builder().build()
    }

    val movieListAdapter: MovieListAdapter = MovieListAdapter(this)

    val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(this)

    var isLoading: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list_layout)
        presentationComponent.inject(this)
        ButterKnife.bind(this)
        presenter.attachView(this)
        initialize()

    }

    private fun initialize() {
        recyclerView.run {
            adapter = movieListAdapter
            layoutManager = linearLayoutManager
            addOnScrollListener(RecyclerViewScrollListener())
        }
        presenter.fetchMovieListFirstPage()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onClick(view: View?) {
        movieListNavigator.navigateToMovieDetails(this, view?.tag as Long)
    }

    override fun addToMovieList(movieList: List<MovieModel>) {
        movieListAdapter.addToMovieList(movieList)
    }

    override fun showError(@StringRes errorMessage: Int) {
        Snackbar.make(rootLayout, errorMessage, Snackbar.LENGTH_LONG).show()
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
        isLoading = true
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
        isLoading = false
    }

    inner class RecyclerViewScrollListener : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            val visibleItemCount = linearLayoutManager.childCount
            val totalItemCount = linearLayoutManager.itemCount
            val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()

            if (visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                presenter.fetchMovieListNextPage()
            }
        }
    }
}
