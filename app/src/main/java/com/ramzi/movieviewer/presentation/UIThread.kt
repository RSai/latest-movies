package com.ramzi.movieviewer.presentation

import com.ramzi.movieviewer.domain.executor.PostExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Singleton

/**
 * This class provides the main UI thread for {@link com.ramzi.movieviewer.domain.usecase.base.BaseReactiveUseCase}
 */
@Singleton
class UIThread : PostExecutionThread {

    override fun getScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}
