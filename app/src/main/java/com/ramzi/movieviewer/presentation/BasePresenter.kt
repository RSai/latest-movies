package com.ramzi.movieviewer.presentation

import android.support.annotation.CallSuper

/**
 * Base class that implements the Presenter interface and provides a base implementation for
 * attachView() and detachView(). It also handles keeping a reference to mvpView which
 * can be accessed in child classes.
 */
open class BasePresenter<VIEW : MvpView> : PresenterInterface<VIEW> {

    var mvpView: VIEW? = null
        private set

    @CallSuper
    override fun attachView(mvpView: VIEW) {
        this.mvpView = mvpView
    }

    @CallSuper
    override fun detachView() {
        this.mvpView = null
    }
}
