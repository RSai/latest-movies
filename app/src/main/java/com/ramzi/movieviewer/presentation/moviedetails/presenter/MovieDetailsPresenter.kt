package com.ramzi.movieviewer.presentation.moviedetails.presenter

import android.util.Log
import com.ramzi.movieviewer.R
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.domain.usecase.GetMovieDetailsUseCase
import com.ramzi.movieviewer.presentation.BasePresenter
import com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject
import javax.inject.Singleton

/**
 * This is the presenter for the Movie Details activity.
 */
@Singleton
class MovieDetailsPresenter
@Inject constructor(private val getMovieDetailsUseCase: GetMovieDetailsUseCase) : BasePresenter<MovieDetailsContract.View>(), MovieDetailsContract.Presenter {

    override fun fetchMovieDetails(id: Long) {
        getMovieDetailsUseCase.execute(MovieDetailsObserver(), id)
        mvpView?.showProgressBar()
    }

    inner class MovieDetailsObserver : DisposableSingleObserver<MovieModel>() {
        override fun onSuccess(movieModel: MovieModel) {
            mvpView?.showMovieDetails(movieModel)
            mvpView?.hideProgressBar()
        }

        override fun onError(e: Throwable) {
            Log.e(MovieDetailsPresenter::class.simpleName, e.toString())
            mvpView?.showError(R.string.network_error_message)
            mvpView?.hideProgressBar()
        }
    }
}
