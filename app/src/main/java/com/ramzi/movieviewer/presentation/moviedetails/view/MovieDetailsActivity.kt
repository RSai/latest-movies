package com.ramzi.movieviewer.presentation.moviedetails.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.widget.ContentLoadingProgressBar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.ramzi.movieviewer.R
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.presentation.common.loadNetworkImage
import com.ramzi.movieviewer.presentation.di.component.DaggerPresentationComponent
import com.ramzi.movieviewer.presentation.di.component.PresentationComponent
import com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract
import javax.inject.Inject

class MovieDetailsActivity : AppCompatActivity(), MovieDetailsContract.View {

    companion object {
        const val MOVIE_ID_EXTRA = "movieIdExtra"

        @JvmStatic
        fun getStartIntent(context: Context, movieId: Long): Intent {
            var intent = Intent(context, MovieDetailsActivity::class.java)
            intent.putExtra(MOVIE_ID_EXTRA, movieId)
            return intent
        }
    }

    @Inject
    lateinit var presenter: MovieDetailsContract.Presenter

    @BindView(R.id.movie_details_activity_root_layout)
    lateinit var rootLayout: ConstraintLayout

    @BindView(R.id.movie_details_activity_progress_bar)
    lateinit var progressBar: ContentLoadingProgressBar

    @BindView(R.id.movie_details_activity_title)
    lateinit var titleTextView: TextView

    @BindView(R.id.movie_details_activity_tag_line)
    lateinit var tagLineTextView: TextView

    @BindView(R.id.movie_details_activity_poster)
    lateinit var posterImageView: ImageView

    @BindView(R.id.movie_details_activity_genres)
    lateinit var genresTextView: TextView

    @BindView(R.id.movie_details_activity_release_date)
    lateinit var releaseDateTextView: TextView

    @BindView(R.id.movie_details_activity_overview)
    lateinit var overviewTextView: TextView

    val presentationComponent: PresentationComponent by lazy {
        DaggerPresentationComponent.builder().build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details_layout)
        presentationComponent.inject(this)
        ButterKnife.bind(this)
        presenter.attachView(this)
        initialize()

    }

    private fun initialize() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_24px)
        var movieId = intent.getLongExtra(MOVIE_ID_EXTRA, 0)
        presenter.fetchMovieDetails(movieId)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    override fun showMovieDetails(movieModel: MovieModel) {
        movieModel.run {
            if (title == null) titleTextView.visibility = View.GONE
            if (releaseDate == null) releaseDateTextView.visibility = View.GONE
            if (tagLine == null) tagLineTextView.visibility = View.GONE
            if (genres == null) genresTextView.visibility = View.GONE

            titleTextView.text = title
            releaseDateTextView.text = releaseDate
            tagLineTextView.text = tagLine
            genresTextView.text = genres
            overviewTextView.text = overview
            posterImageView.loadNetworkImage(getString(R.string.movie_poster_path, posterPath))
            posterImageView
        }
    }

    override fun showError(@StringRes errorMessage: Int) {
        Snackbar.make(rootLayout, errorMessage, Snackbar.LENGTH_LONG).show()
    }

    override fun showProgressBar() {
        progressBar.show()
    }

    override fun hideProgressBar() {
        progressBar.hide()
    }
}
