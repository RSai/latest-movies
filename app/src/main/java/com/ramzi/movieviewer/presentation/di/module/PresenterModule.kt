package com.ramzi.movieviewer.presentation.di.module

import com.ramzi.movieviewer.domain.usecase.GetMovieDetailsUseCase
import com.ramzi.movieviewer.domain.usecase.GetMovieListUseCase
import com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract
import com.ramzi.movieviewer.presentation.moviedetails.presenter.MovieDetailsPresenter
import com.ramzi.movieviewer.presentation.movielist.MovieListContract
import com.ramzi.movieviewer.presentation.movielist.presenter.MovieListPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun provideMovieListPresenter(getMovieListUseCase: GetMovieListUseCase): MovieListContract.Presenter = MovieListPresenter(getMovieListUseCase)

    @Provides
    @Singleton
    fun provideMovieDetailsPresenter(getMovieDetailsUseCase: GetMovieDetailsUseCase): MovieDetailsContract.Presenter = MovieDetailsPresenter(getMovieDetailsUseCase)
}