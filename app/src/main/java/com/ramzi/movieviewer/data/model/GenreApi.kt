package com.ramzi.movieviewer.data.model

import com.google.gson.annotations.SerializedName

class GenreApi(
        @field:SerializedName("id") val id: Long,
        @field:SerializedName("name") val name: String?
)