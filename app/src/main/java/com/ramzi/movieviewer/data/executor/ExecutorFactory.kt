package com.ramzi.movieviewer.data.executor

import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject

import javax.inject.Singleton

/**
 * Creates [Executor]s
 */
@Singleton
class ExecutorFactory
@Inject constructor() {

    /**
     * @see Executors.newFixedThreadPool
     */
    fun createFixedThreadPoolExecutor(pool: Int): Executor {
        return Executors.newFixedThreadPool(pool)
    }

}