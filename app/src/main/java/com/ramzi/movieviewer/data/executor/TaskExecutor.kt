package com.ramzi.movieviewer.data.executor

import com.ramzi.movieviewer.domain.executor.ThreadExecutor

import java.util.concurrent.Executor

import javax.inject.Inject
import javax.inject.Singleton

/**
 * This class define the task executor which is responsible for
 * creating appropriate pool of threads for executing
 * background jobs in [com.ramzi.movieviewer.domain.usecase.base.BaseReactiveUseCase].
 */
@Singleton
class TaskExecutor
@Inject constructor(executorFactory: ExecutorFactory) : ThreadExecutor {

    companion object {

        internal val THREAD_POOL = 3
    }

    private val threadPoolExecutor: Executor

    init {
        threadPoolExecutor = executorFactory.createFixedThreadPoolExecutor(THREAD_POOL)
    }

    override fun execute(runnable: Runnable) {
        threadPoolExecutor.execute(runnable)
    }

}
