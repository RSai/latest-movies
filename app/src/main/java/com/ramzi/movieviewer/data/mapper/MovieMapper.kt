package com.ramzi.movieviewer.data.mapper

import com.ramzi.movieviewer.data.model.DiscoverMovieListApi
import com.ramzi.movieviewer.data.model.MovieApi
import com.ramzi.movieviewer.domain.model.MovieModel
import javax.inject.Inject

/**
 * This class is responsible for mapping models between the data and domain layers in clean architecture
 */
class MovieMapper
@Inject constructor() {

    fun transform(api: DiscoverMovieListApi): List<MovieModel> {
        val movieModelList = mutableListOf<MovieModel>()
        for (movieApi in api.results) {
            val genreString = movieApi.genres?.joinToString { genre -> genre.toString() }
            movieModelList.add(MovieModel(id = movieApi.id, title = movieApi.title, releaseDate = movieApi.releaseDate, genres = genreString))
        }
        return movieModelList
    }

    fun transform(api: MovieApi): MovieModel {
        val genreString = api.genres?.joinToString { genre -> genre.name.toString() }
        return MovieModel(id = api.id,
                title = api.title,
                releaseDate = api.releaseDate,
                genres = genreString,
                overview = api.overview,
                tagLine = api.tagLine,
                originalLanguage = api.originalLanguage,
                posterPath = api.posterPath)
    }
}