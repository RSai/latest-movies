package com.ramzi.movieviewer.data.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

/**
 * This class is used to initialize Retrofit
 */
class RetrofitHelper
@Inject constructor() {

    companion object {
        const val API_KEY = "112e41701000b291a09eae5ef4cd6727"
        const val BASE_URL = "https://api.themoviedb.org/3/"
    }

    val clientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

    val gson: Gson by lazy {
        GsonBuilder()
                .setLenient()
                .create()
    }

    val retrofit: Retrofit by lazy {
        Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(clientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    init {
        clientBuilder.addInterceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("api_key", API_KEY)
                    .build()

            val requestBuilder = original.newBuilder().url(url)

            val request = requestBuilder.build()
            chain.proceed(request)
        }
    }
}