package com.ramzi.movieviewer.data.network.request

import com.ramzi.movieviewer.data.model.DiscoverMovieListApi
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface DiscoverMovieRequest {

    @GET("discover/movie?language=en-US&include_adult=false&include_video=false&sort_by=release_date.desc")
    fun fetchNewMovies(@Query("page") page: Int?,
                       @Query("release_date.lte") date: Long): Observable<DiscoverMovieListApi>
}