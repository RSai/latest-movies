package com.ramzi.movieviewer.data.network.request

import com.ramzi.movieviewer.data.model.MovieApi
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieDetailsRequest {


    @GET("movie/{movie_id}?language=en-US")
    fun fetchMovieDetails(@Path("movie_id") id: Long?): Observable<MovieApi>
}