package com.ramzi.movieviewer.data.model

import com.google.gson.annotations.SerializedName

class MovieApi(
        @field:SerializedName("id") val id: Long,
        @field:SerializedName("title") val title: String? = null,
        @field:SerializedName("release_date") val releaseDate: String? = null,
        @field:SerializedName("overview") val overview: String? = null,
        @field:SerializedName("original_language") val originalLanguage: String? = null,
        @field:SerializedName("genres") val genres: MutableList<GenreApi>? = null,
        @field:SerializedName("tagline") val tagLine: String? = null,
        @field:SerializedName("poster_path") val posterPath: String? = null
)
