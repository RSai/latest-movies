package com.ramzi.movieviewer.data.model

import com.google.gson.annotations.SerializedName

class DiscoverMovieListApi(
        @field:SerializedName("page") val page: Int,
        @field:SerializedName("results") val results: List<MovieApi>
)