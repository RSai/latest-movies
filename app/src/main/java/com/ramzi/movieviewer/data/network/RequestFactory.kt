package com.ramzi.movieviewer.data.network

import com.ramzi.movieviewer.data.network.request.DiscoverMovieRequest
import com.ramzi.movieviewer.data.network.request.MovieDetailsRequest
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RequestFactory
@Inject constructor(private val retrofitHelper: RetrofitHelper) {

    fun getDiscoverMovieRequest(): DiscoverMovieRequest {
        return retrofitHelper.retrofit.create(DiscoverMovieRequest::class.java)
    }

    fun getMovieDetailsRequest(): MovieDetailsRequest {
        return retrofitHelper.retrofit.create(MovieDetailsRequest::class.java)
    }
}
