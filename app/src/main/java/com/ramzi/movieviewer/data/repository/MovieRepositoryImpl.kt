package com.ramzi.movieviewer.data.repository

import com.ramzi.movieviewer.data.mapper.MovieMapper
import com.ramzi.movieviewer.data.network.RequestFactory
import com.ramzi.movieviewer.domain.model.MovieModel
import com.ramzi.movieviewer.domain.repository.MovieRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieRepositoryImpl
@Inject constructor(private val requestFactory: RequestFactory,
                    private val movieMapper: MovieMapper) : MovieRepository {

    override fun getLatestMovies(page: Int?): Single<List<MovieModel>> {
        return requestFactory.getDiscoverMovieRequest().fetchNewMovies(page, System.currentTimeMillis())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).map {
                    movieMapper.transform(it)
                }.singleOrError()
    }

    override fun getMovieDetails(id: Long?): Single<MovieModel> {
        return requestFactory.getMovieDetailsRequest().fetchMovieDetails(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).map {
                    movieMapper.transform(it)
                }.singleOrError()
    }
}