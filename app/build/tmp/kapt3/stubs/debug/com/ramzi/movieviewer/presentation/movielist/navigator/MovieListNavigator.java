package com.ramzi.movieviewer.presentation.movielist.navigator;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/navigator/MovieListNavigator;", "", "()V", "navigateToMovieDetails", "", "context", "Landroid/content/Context;", "id", "", "(Landroid/content/Context;Ljava/lang/Long;)V", "app_debug"})
public final class MovieListNavigator {
    
    public final void navigateToMovieDetails(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.Long id) {
    }
    
    @javax.inject.Inject()
    public MovieListNavigator() {
        super();
    }
}