package com.ramzi.movieviewer.presentation.moviedetails.view;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 F2\u00020\u00012\u00020\u0002:\u0001FB\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u00104\u001a\u000205H\u0016J\b\u00106\u001a\u000205H\u0002J\u0012\u00107\u001a\u0002052\b\u00108\u001a\u0004\u0018\u000109H\u0014J\b\u0010:\u001a\u000205H\u0014J\u0012\u0010;\u001a\u00020<2\b\u0010=\u001a\u0004\u0018\u00010>H\u0016J\u0012\u0010?\u001a\u0002052\b\b\u0001\u0010@\u001a\u00020AH\u0016J\u0010\u0010B\u001a\u0002052\u0006\u0010C\u001a\u00020DH\u0016J\b\u0010E\u001a\u000205H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001e\u0010\n\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\tR\u001e\u0010\r\u001a\u00020\u000e8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u00020\u00148FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0015\u0010\u0016R\u001e\u0010\u0019\u001a\u00020\u001a8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\u00020 8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001e\u0010%\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0007\"\u0004\b\'\u0010\tR\u001e\u0010(\u001a\u00020)8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001e\u0010.\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\u0007\"\u0004\b0\u0010\tR\u001e\u00101\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u0007\"\u0004\b3\u0010\t\u00a8\u0006G"}, d2 = {"Lcom/ramzi/movieviewer/presentation/moviedetails/view/MovieDetailsActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$View;", "()V", "genresTextView", "Landroid/widget/TextView;", "getGenresTextView", "()Landroid/widget/TextView;", "setGenresTextView", "(Landroid/widget/TextView;)V", "overviewTextView", "getOverviewTextView", "setOverviewTextView", "posterImageView", "Landroid/widget/ImageView;", "getPosterImageView", "()Landroid/widget/ImageView;", "setPosterImageView", "(Landroid/widget/ImageView;)V", "presentationComponent", "Lcom/ramzi/movieviewer/presentation/di/component/PresentationComponent;", "getPresentationComponent", "()Lcom/ramzi/movieviewer/presentation/di/component/PresentationComponent;", "presentationComponent$delegate", "Lkotlin/Lazy;", "presenter", "Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$Presenter;", "getPresenter", "()Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$Presenter;", "setPresenter", "(Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$Presenter;)V", "progressBar", "Landroid/support/v4/widget/ContentLoadingProgressBar;", "getProgressBar", "()Landroid/support/v4/widget/ContentLoadingProgressBar;", "setProgressBar", "(Landroid/support/v4/widget/ContentLoadingProgressBar;)V", "releaseDateTextView", "getReleaseDateTextView", "setReleaseDateTextView", "rootLayout", "Landroid/support/constraint/ConstraintLayout;", "getRootLayout", "()Landroid/support/constraint/ConstraintLayout;", "setRootLayout", "(Landroid/support/constraint/ConstraintLayout;)V", "tagLineTextView", "getTagLineTextView", "setTagLineTextView", "titleTextView", "getTitleTextView", "setTitleTextView", "hideProgressBar", "", "initialize", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "showError", "errorMessage", "", "showMovieDetails", "movieModel", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "showProgressBar", "Companion", "app_debug"})
public final class MovieDetailsActivity extends android.support.v7.app.AppCompatActivity implements com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.View {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.Presenter presenter;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_root_layout)
    public android.support.constraint.ConstraintLayout rootLayout;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_progress_bar)
    public android.support.v4.widget.ContentLoadingProgressBar progressBar;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_title)
    public android.widget.TextView titleTextView;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_tag_line)
    public android.widget.TextView tagLineTextView;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_poster)
    public android.widget.ImageView posterImageView;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_genres)
    public android.widget.TextView genresTextView;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_release_date)
    public android.widget.TextView releaseDateTextView;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_details_activity_overview)
    public android.widget.TextView overviewTextView;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy presentationComponent$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MOVIE_ID_EXTRA = "movieIdExtra";
    public static final com.ramzi.movieviewer.presentation.moviedetails.view.MovieDetailsActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.Presenter getPresenter() {
        return null;
    }
    
    public final void setPresenter(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.Presenter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.support.constraint.ConstraintLayout getRootLayout() {
        return null;
    }
    
    public final void setRootLayout(@org.jetbrains.annotations.NotNull()
    android.support.constraint.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.support.v4.widget.ContentLoadingProgressBar getProgressBar() {
        return null;
    }
    
    public final void setProgressBar(@org.jetbrains.annotations.NotNull()
    android.support.v4.widget.ContentLoadingProgressBar p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getTitleTextView() {
        return null;
    }
    
    public final void setTitleTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getTagLineTextView() {
        return null;
    }
    
    public final void setTagLineTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.ImageView getPosterImageView() {
        return null;
    }
    
    public final void setPosterImageView(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getGenresTextView() {
        return null;
    }
    
    public final void setGenresTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getReleaseDateTextView() {
        return null;
    }
    
    public final void setReleaseDateTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getOverviewTextView() {
        return null;
    }
    
    public final void setOverviewTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.presentation.di.component.PresentationComponent getPresentationComponent() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initialize() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.Nullable()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void showMovieDetails(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.model.MovieModel movieModel) {
    }
    
    @java.lang.Override()
    public void showError(@android.support.annotation.StringRes()
    int errorMessage) {
    }
    
    @java.lang.Override()
    public void showProgressBar() {
    }
    
    @java.lang.Override()
    public void hideProgressBar() {
    }
    
    public MovieDetailsActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.content.Intent getStartIntent(@org.jetbrains.annotations.NotNull()
    android.content.Context context, long movieId) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/ramzi/movieviewer/presentation/moviedetails/view/MovieDetailsActivity$Companion;", "", "()V", "MOVIE_ID_EXTRA", "", "getStartIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "movieId", "", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Intent getStartIntent(@org.jetbrains.annotations.NotNull()
        android.content.Context context, long movieId) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}