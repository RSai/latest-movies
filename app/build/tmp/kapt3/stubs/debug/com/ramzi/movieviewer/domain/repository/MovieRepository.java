package com.ramzi.movieviewer.domain.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J#\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H&\u00a2\u0006\u0002\u0010\bJ\u001d\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH&\u00a2\u0006\u0002\u0010\f\u00a8\u0006\r"}, d2 = {"Lcom/ramzi/movieviewer/domain/repository/MovieRepository;", "", "getLatestMovies", "Lio/reactivex/Single;", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "page", "", "(Ljava/lang/Integer;)Lio/reactivex/Single;", "getMovieDetails", "id", "", "(Ljava/lang/Long;)Lio/reactivex/Single;", "app_debug"})
public abstract interface MovieRepository {
    
    @org.jetbrains.annotations.NotNull()
    public abstract io.reactivex.Single<java.util.List<com.ramzi.movieviewer.domain.model.MovieModel>> getLatestMovies(@org.jetbrains.annotations.Nullable()
    java.lang.Integer page);
    
    @org.jetbrains.annotations.NotNull()
    public abstract io.reactivex.Single<com.ramzi.movieviewer.domain.model.MovieModel> getMovieDetails(@org.jetbrains.annotations.Nullable()
    java.lang.Long id);
}