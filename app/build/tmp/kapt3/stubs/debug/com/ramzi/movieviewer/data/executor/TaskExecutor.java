package com.ramzi.movieviewer.data.executor;

import java.lang.System;

/**
 * * This class define task executor which is responsible for
 * * creating appropriate pool of threads for executing
 * * background jobs in [com.ramzi.movieviewer.domain.usecase.base.BaseReactiveUseCase].
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/ramzi/movieviewer/data/executor/TaskExecutor;", "Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;", "executorFactory", "Lcom/ramzi/movieviewer/data/executor/ExecutorFactory;", "(Lcom/ramzi/movieviewer/data/executor/ExecutorFactory;)V", "threadPoolExecutor", "Ljava/util/concurrent/Executor;", "execute", "", "runnable", "Ljava/lang/Runnable;", "Companion", "app_debug"})
@javax.inject.Singleton()
public final class TaskExecutor implements com.ramzi.movieviewer.domain.executor.ThreadExecutor {
    private final java.util.concurrent.Executor threadPoolExecutor = null;
    private static final int THREAD_POOL = 3;
    public static final com.ramzi.movieviewer.data.executor.TaskExecutor.Companion Companion = null;
    
    @java.lang.Override()
    public void execute(@org.jetbrains.annotations.NotNull()
    java.lang.Runnable runnable) {
    }
    
    @javax.inject.Inject()
    public TaskExecutor(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.executor.ExecutorFactory executorFactory) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0080D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/ramzi/movieviewer/data/executor/TaskExecutor$Companion;", "", "()V", "THREAD_POOL", "", "getTHREAD_POOL$app_debug", "()I", "app_debug"})
    public static final class Companion {
        
        public final int getTHREAD_POOL$app_debug() {
            return 0;
        }
        
        private Companion() {
            super();
        }
    }
}