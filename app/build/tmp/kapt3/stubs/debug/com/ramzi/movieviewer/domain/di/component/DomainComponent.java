package com.ramzi.movieviewer.domain.di.component;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/ramzi/movieviewer/domain/di/component/DomainComponent;", "", "provideGetMovieDetailsUseCase", "Lcom/ramzi/movieviewer/domain/usecase/GetMovieDetailsUseCase;", "provideGetMovieListUseCase", "Lcom/ramzi/movieviewer/domain/usecase/GetMovieListUseCase;", "provideMovieRepository", "Lcom/ramzi/movieviewer/domain/repository/MovieRepository;", "app_debug"})
@dagger.Component(modules = {com.ramzi.movieviewer.domain.di.module.ExecutorModule.class, com.ramzi.movieviewer.presentation.di.module.RepositoryModule.class})
@javax.inject.Singleton()
public abstract interface DomainComponent {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.ramzi.movieviewer.domain.usecase.GetMovieListUseCase provideGetMovieListUseCase();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.ramzi.movieviewer.domain.usecase.GetMovieDetailsUseCase provideGetMovieDetailsUseCase();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.ramzi.movieviewer.domain.repository.MovieRepository provideMovieRepository();
}