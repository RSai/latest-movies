package com.ramzi.movieviewer.data.network.request;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u001f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\'\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/ramzi/movieviewer/data/network/request/MovieDetailsRequest;", "", "fetchMovieDetails", "Lio/reactivex/Observable;", "Lcom/ramzi/movieviewer/data/model/MovieApi;", "id", "", "(Ljava/lang/Long;)Lio/reactivex/Observable;", "app_debug"})
public abstract interface MovieDetailsRequest {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "movie/{movie_id}?language=en-US")
    public abstract io.reactivex.Observable<com.ramzi.movieviewer.data.model.MovieApi> fetchMovieDetails(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Path(value = "movie_id")
    java.lang.Long id);
}