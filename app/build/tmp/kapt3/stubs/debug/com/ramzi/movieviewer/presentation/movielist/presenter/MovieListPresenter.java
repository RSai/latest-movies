package com.ramzi.movieviewer.presentation.movielist.presenter;

import java.lang.System;

/**
 * * This is the presenter for the Movie List activity
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001\rB\u000f\b\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u000bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u000e"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/presenter/MovieListPresenter;", "Lcom/ramzi/movieviewer/presentation/BasePresenter;", "Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$View;", "Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$Presenter;", "getMovieListUseCase", "Lcom/ramzi/movieviewer/domain/usecase/GetMovieListUseCase;", "(Lcom/ramzi/movieviewer/domain/usecase/GetMovieListUseCase;)V", "pageNumber", "", "Ljava/lang/Integer;", "fetchMovieListFirstPage", "", "fetchMovieListNextPage", "MovieListObserver", "app_debug"})
@javax.inject.Singleton()
public final class MovieListPresenter extends com.ramzi.movieviewer.presentation.BasePresenter<com.ramzi.movieviewer.presentation.movielist.MovieListContract.View> implements com.ramzi.movieviewer.presentation.movielist.MovieListContract.Presenter {
    private java.lang.Integer pageNumber;
    private final com.ramzi.movieviewer.domain.usecase.GetMovieListUseCase getMovieListUseCase = null;
    
    @java.lang.Override()
    public void fetchMovieListFirstPage() {
    }
    
    @java.lang.Override()
    public void fetchMovieListNextPage() {
    }
    
    @javax.inject.Inject()
    public MovieListPresenter(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.usecase.GetMovieListUseCase getMovieListUseCase) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\b\u0086\u0004\u0018\u00002\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0016\u0010\t\u001a\u00020\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0016\u00a8\u0006\u000b"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/presenter/MovieListPresenter$MovieListObserver;", "Lio/reactivex/observers/DisposableSingleObserver;", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "(Lcom/ramzi/movieviewer/presentation/movielist/presenter/MovieListPresenter;)V", "onError", "", "e", "", "onSuccess", "movieList", "app_debug"})
    public final class MovieListObserver extends io.reactivex.observers.DisposableSingleObserver<java.util.List<? extends com.ramzi.movieviewer.domain.model.MovieModel>> {
        
        @java.lang.Override()
        public void onSuccess(@org.jetbrains.annotations.NotNull()
        java.util.List<com.ramzi.movieviewer.domain.model.MovieModel> movieList) {
        }
        
        @java.lang.Override()
        public void onError(@org.jetbrains.annotations.NotNull()
        java.lang.Throwable e) {
        }
        
        public MovieListObserver() {
            super();
        }
    }
}