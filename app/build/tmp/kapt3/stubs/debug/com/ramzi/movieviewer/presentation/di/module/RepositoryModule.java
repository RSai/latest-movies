package com.ramzi.movieviewer.presentation.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007\u00a8\u0006\t"}, d2 = {"Lcom/ramzi/movieviewer/presentation/di/module/RepositoryModule;", "", "()V", "provideMovieRepository", "Lcom/ramzi/movieviewer/domain/repository/MovieRepository;", "requestFactory", "Lcom/ramzi/movieviewer/data/network/RequestFactory;", "movieMapper", "Lcom/ramzi/movieviewer/data/mapper/MovieMapper;", "app_debug"})
@dagger.Module()
public final class RepositoryModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.ramzi.movieviewer.domain.repository.MovieRepository provideMovieRepository(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.network.RequestFactory requestFactory, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.mapper.MovieMapper movieMapper) {
        return null;
    }
    
    public RepositoryModule() {
        super();
    }
}