package com.ramzi.movieviewer.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J#\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016\u00a2\u0006\u0002\u0010\rJ\u001d\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016\u00a2\u0006\u0002\u0010\u0011R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/ramzi/movieviewer/data/repository/MovieRepositoryImpl;", "Lcom/ramzi/movieviewer/domain/repository/MovieRepository;", "requestFactory", "Lcom/ramzi/movieviewer/data/network/RequestFactory;", "movieMapper", "Lcom/ramzi/movieviewer/data/mapper/MovieMapper;", "(Lcom/ramzi/movieviewer/data/network/RequestFactory;Lcom/ramzi/movieviewer/data/mapper/MovieMapper;)V", "getLatestMovies", "Lio/reactivex/Single;", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "page", "", "(Ljava/lang/Integer;)Lio/reactivex/Single;", "getMovieDetails", "id", "", "(Ljava/lang/Long;)Lio/reactivex/Single;", "app_debug"})
public final class MovieRepositoryImpl implements com.ramzi.movieviewer.domain.repository.MovieRepository {
    private final com.ramzi.movieviewer.data.network.RequestFactory requestFactory = null;
    private final com.ramzi.movieviewer.data.mapper.MovieMapper movieMapper = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Single<java.util.List<com.ramzi.movieviewer.domain.model.MovieModel>> getLatestMovies(@org.jetbrains.annotations.Nullable()
    java.lang.Integer page) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Single<com.ramzi.movieviewer.domain.model.MovieModel> getMovieDetails(@org.jetbrains.annotations.Nullable()
    java.lang.Long id) {
        return null;
    }
    
    @javax.inject.Inject()
    public MovieRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.network.RequestFactory requestFactory, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.mapper.MovieMapper movieMapper) {
        super();
    }
}