package com.ramzi.movieviewer.presentation;

import java.lang.System;

/**
 * * Base interface that any class that wants to act as a View in the MVP (Model View Presenter) pattern must implement.
 * * Generally this interface will be extended by a more specific interface that then usually will be implemented by an Activity or Fragment.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\bf\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/ramzi/movieviewer/presentation/MvpView;", "", "app_debug"})
public abstract interface MvpView {
}