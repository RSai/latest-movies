package com.ramzi.movieviewer.domain.usecase;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0004\u0012\u00020\u00040\u0001B\'\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ#\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0016\u00a2\u0006\u0002\u0010\u0011R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/ramzi/movieviewer/domain/usecase/GetMovieListUseCase;", "Lcom/ramzi/movieviewer/domain/usecase/base/SingleUseCase;", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "", "threadExecutor", "Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;", "postExecutionThread", "Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;", "rxFactory", "Lcom/ramzi/movieviewer/domain/rx/RxFactory;", "movieRepository", "Lcom/ramzi/movieviewer/domain/repository/MovieRepository;", "(Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;Lcom/ramzi/movieviewer/domain/rx/RxFactory;Lcom/ramzi/movieviewer/domain/repository/MovieRepository;)V", "buildUseCaseSingle", "Lio/reactivex/Single;", "params", "(Ljava/lang/Integer;)Lio/reactivex/Single;", "app_debug"})
public final class GetMovieListUseCase extends com.ramzi.movieviewer.domain.usecase.base.SingleUseCase<java.util.List<? extends com.ramzi.movieviewer.domain.model.MovieModel>, java.lang.Integer> {
    private final com.ramzi.movieviewer.domain.repository.MovieRepository movieRepository = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Single<java.util.List<com.ramzi.movieviewer.domain.model.MovieModel>> buildUseCaseSingle(@org.jetbrains.annotations.Nullable()
    java.lang.Integer params) {
        return null;
    }
    
    @javax.inject.Inject()
    public GetMovieListUseCase(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.executor.ThreadExecutor threadExecutor, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.executor.PostExecutionThread postExecutionThread, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.rx.RxFactory rxFactory, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.repository.MovieRepository movieRepository) {
        super(null, null, null);
    }
}