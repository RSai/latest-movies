package com.ramzi.movieviewer.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/ramzi/movieviewer/data/model/DiscoverMovieListApi;", "", "page", "", "results", "", "Lcom/ramzi/movieviewer/data/model/MovieApi;", "(ILjava/util/List;)V", "getPage", "()I", "getResults", "()Ljava/util/List;", "app_debug"})
public final class DiscoverMovieListApi {
    @com.google.gson.annotations.SerializedName(value = "page")
    private final int page = 0;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "results")
    private final java.util.List<com.ramzi.movieviewer.data.model.MovieApi> results = null;
    
    public final int getPage() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.ramzi.movieviewer.data.model.MovieApi> getResults() {
        return null;
    }
    
    public DiscoverMovieListApi(int page, @org.jetbrains.annotations.NotNull()
    java.util.List<com.ramzi.movieviewer.data.model.MovieApi> results) {
        super();
    }
}