package com.ramzi.movieviewer.data.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/ramzi/movieviewer/data/network/RequestFactory;", "", "retrofitHelper", "Lcom/ramzi/movieviewer/data/network/RetrofitHelper;", "(Lcom/ramzi/movieviewer/data/network/RetrofitHelper;)V", "getDiscoverMovieRequest", "Lcom/ramzi/movieviewer/data/network/request/DiscoverMovieRequest;", "getMovieDetailsRequest", "Lcom/ramzi/movieviewer/data/network/request/MovieDetailsRequest;", "app_debug"})
@javax.inject.Singleton()
public final class RequestFactory {
    private final com.ramzi.movieviewer.data.network.RetrofitHelper retrofitHelper = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.data.network.request.DiscoverMovieRequest getDiscoverMovieRequest() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.data.network.request.MovieDetailsRequest getMovieDetailsRequest() {
        return null;
    }
    
    @javax.inject.Inject()
    public RequestFactory(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.network.RetrofitHelper retrofitHelper) {
        super();
    }
}