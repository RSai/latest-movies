package com.ramzi.movieviewer.domain.usecase.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0004J\b\u0010\u0015\u001a\u00020\u0012H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\fX\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\fX\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000e\u00a8\u0006\u0016"}, d2 = {"Lcom/ramzi/movieviewer/domain/usecase/base/BaseReactiveUseCase;", "", "threadExecutor", "Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;", "postExecutionThread", "Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;", "rxFactory", "Lcom/ramzi/movieviewer/domain/rx/RxFactory;", "(Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;Lcom/ramzi/movieviewer/domain/rx/RxFactory;)V", "disposables", "Lio/reactivex/disposables/CompositeDisposable;", "postExecutionThreadScheduler", "Lio/reactivex/Scheduler;", "getPostExecutionThreadScheduler", "()Lio/reactivex/Scheduler;", "threadExecutorScheduler", "getThreadExecutorScheduler", "addDisposable", "", "disposable", "Lio/reactivex/disposables/Disposable;", "dispose", "app_debug"})
public abstract class BaseReactiveUseCase {
    @org.jetbrains.annotations.NotNull()
    private final io.reactivex.Scheduler threadExecutorScheduler = null;
    @org.jetbrains.annotations.NotNull()
    private final io.reactivex.Scheduler postExecutionThreadScheduler = null;
    private final io.reactivex.disposables.CompositeDisposable disposables = null;
    
    @org.jetbrains.annotations.NotNull()
    protected final io.reactivex.Scheduler getThreadExecutorScheduler() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final io.reactivex.Scheduler getPostExecutionThreadScheduler() {
        return null;
    }
    
    /**
     * * Dispose from current [CompositeDisposable].
     */
    public void dispose() {
    }
    
    /**
     * * Dispose from current [CompositeDisposable].
     */
    protected final void addDisposable(@org.jetbrains.annotations.NotNull()
    io.reactivex.disposables.Disposable disposable) {
    }
    
    public BaseReactiveUseCase(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.executor.ThreadExecutor threadExecutor, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.executor.PostExecutionThread postExecutionThread, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.rx.RxFactory rxFactory) {
        super();
    }
}