package com.ramzi.movieviewer.presentation.di.component;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\b\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\f"}, d2 = {"Lcom/ramzi/movieviewer/presentation/di/component/PresentationComponent;", "", "inject", "", "movieDetailsActivity", "Lcom/ramzi/movieviewer/presentation/moviedetails/view/MovieDetailsActivity;", "movieListActivity", "Lcom/ramzi/movieviewer/presentation/movielist/view/MovieListActivity;", "provideMovieDetailsPresenter", "Lcom/ramzi/movieviewer/presentation/moviedetails/presenter/MovieDetailsPresenter;", "provideMovieListPresenter", "Lcom/ramzi/movieviewer/presentation/movielist/presenter/MovieListPresenter;", "app_debug"})
@dagger.Component(modules = {com.ramzi.movieviewer.presentation.di.module.PresenterModule.class, com.ramzi.movieviewer.domain.di.module.ExecutorModule.class, com.ramzi.movieviewer.presentation.di.module.RepositoryModule.class})
@javax.inject.Singleton()
public abstract interface PresentationComponent {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.ramzi.movieviewer.presentation.movielist.presenter.MovieListPresenter provideMovieListPresenter();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.ramzi.movieviewer.presentation.moviedetails.presenter.MovieDetailsPresenter provideMovieDetailsPresenter();
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.presentation.movielist.view.MovieListActivity movieListActivity);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.presentation.moviedetails.view.MovieDetailsActivity movieDetailsActivity);
}