package com.ramzi.movieviewer.presentation.movielist;

import java.lang.System;

/**
 * * Interfaces for Movie list view and presenter.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001:\u0002\u0002\u0003\u00a8\u0006\u0004"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract;", "", "Presenter", "View", "app_debug"})
public abstract interface MovieListContract {
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\'J\b\u0010\u0007\u001a\u00020\u0003H\'J\u0012\u0010\b\u001a\u00020\u00032\b\b\u0001\u0010\t\u001a\u00020\nH\'J\b\u0010\u000b\u001a\u00020\u0003H\'\u00a8\u0006\f"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$View;", "Lcom/ramzi/movieviewer/presentation/MvpView;", "addToMovieList", "", "movieList", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "hideProgressBar", "showError", "errorMessage", "", "showProgressBar", "app_debug"})
    public static abstract interface View extends com.ramzi.movieviewer.presentation.MvpView {
        
        @android.support.annotation.UiThread()
        public abstract void addToMovieList(@org.jetbrains.annotations.NotNull()
        java.util.List<com.ramzi.movieviewer.domain.model.MovieModel> movieList);
        
        @android.support.annotation.UiThread()
        public abstract void showError(@android.support.annotation.StringRes()
        int errorMessage);
        
        @android.support.annotation.UiThread()
        public abstract void showProgressBar();
        
        @android.support.annotation.UiThread()
        public abstract void hideProgressBar();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&\u00a8\u0006\u0006"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$Presenter;", "Lcom/ramzi/movieviewer/presentation/PresenterInterface;", "Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$View;", "fetchMovieListFirstPage", "", "fetchMovieListNextPage", "app_debug"})
    public static abstract interface Presenter extends com.ramzi.movieviewer.presentation.PresenterInterface<com.ramzi.movieviewer.presentation.movielist.MovieListContract.View> {
        
        public abstract void fetchMovieListFirstPage();
        
        public abstract void fetchMovieListNextPage();
    }
}