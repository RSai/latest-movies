package com.ramzi.movieviewer.domain.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007J\b\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007\u00a8\u0006\u000b"}, d2 = {"Lcom/ramzi/movieviewer/domain/di/module/ExecutorModule;", "", "()V", "providePostExecutionThread", "Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;", "provideRxFactory", "Lcom/ramzi/movieviewer/domain/rx/RxFactory;", "provideThreadExecutor", "Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;", "executorFactory", "Lcom/ramzi/movieviewer/data/executor/ExecutorFactory;", "app_debug"})
@dagger.Module()
public final class ExecutorModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.ramzi.movieviewer.domain.executor.PostExecutionThread providePostExecutionThread() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.ramzi.movieviewer.domain.executor.ThreadExecutor provideThreadExecutor(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.executor.ExecutorFactory executorFactory) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.ramzi.movieviewer.domain.rx.RxFactory provideRxFactory() {
        return null;
    }
    
    public ExecutorModule() {
        super();
    }
}