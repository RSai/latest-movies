package com.ramzi.movieviewer.data.network.request;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J)\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bH\'\u00a2\u0006\u0002\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/ramzi/movieviewer/data/network/request/DiscoverMovieRequest;", "", "fetchNewMovies", "Lio/reactivex/Observable;", "Lcom/ramzi/movieviewer/data/model/DiscoverMovieListApi;", "page", "", "date", "", "(Ljava/lang/Integer;J)Lio/reactivex/Observable;", "app_debug"})
public abstract interface DiscoverMovieRequest {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "discover/movie?language=en-US&include_adult=false&include_video=false&sort_by=release_date.desc")
    public abstract io.reactivex.Observable<com.ramzi.movieviewer.data.model.DiscoverMovieListApi> fetchNewMovies(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "page")
    java.lang.Integer page, @retrofit2.http.Query(value = "release_date.lte")
    long date);
}