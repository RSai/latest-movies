package com.ramzi.movieviewer.data.mapper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\u0003\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/ramzi/movieviewer/data/mapper/MovieMapper;", "", "()V", "transform", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "api", "Lcom/ramzi/movieviewer/data/model/DiscoverMovieListApi;", "Lcom/ramzi/movieviewer/data/model/MovieApi;", "app_debug"})
public final class MovieMapper {
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.ramzi.movieviewer.domain.model.MovieModel> transform(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.model.DiscoverMovieListApi api) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.domain.model.MovieModel transform(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.data.model.MovieApi api) {
        return null;
    }
    
    @javax.inject.Inject()
    public MovieMapper() {
        super();
    }
}