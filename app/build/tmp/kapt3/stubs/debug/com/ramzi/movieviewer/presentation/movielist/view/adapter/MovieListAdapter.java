package com.ramzi.movieviewer.presentation.movielist.view.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0014\u0010\t\u001a\u00020\n2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u000bJ\b\u0010\f\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\rH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\rH\u0016R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/view/adapter/MovieListAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lcom/ramzi/movieviewer/presentation/movielist/view/adapter/MovieViewHolder;", "onClickListener", "Landroid/view/View$OnClickListener;", "(Landroid/view/View$OnClickListener;)V", "movieList", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "addToMovieList", "", "", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "app_debug"})
public final class MovieListAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieViewHolder> {
    private java.util.List<com.ramzi.movieviewer.domain.model.MovieModel> movieList;
    private android.view.View.OnClickListener onClickListener;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieViewHolder holder, int position) {
    }
    
    public final void addToMovieList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.ramzi.movieviewer.domain.model.MovieModel> movieList) {
    }
    
    public MovieListAdapter(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener onClickListener) {
        super();
    }
}