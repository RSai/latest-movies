package com.ramzi.movieviewer.presentation.movielist.view.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001e\u0010\u0005\u001a\u00020\u00068\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u00020\u00068\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\b\"\u0004\b\u0013\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/view/adapter/MovieViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "releaseDate", "Landroid/widget/TextView;", "getReleaseDate", "()Landroid/widget/TextView;", "setReleaseDate", "(Landroid/widget/TextView;)V", "rootLayout", "Landroid/widget/LinearLayout;", "getRootLayout", "()Landroid/widget/LinearLayout;", "setRootLayout", "(Landroid/widget/LinearLayout;)V", "title", "getTitle", "setTitle", "Companion", "app_debug"})
public final class MovieViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_list_item_root_layout)
    public android.widget.LinearLayout rootLayout;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_list_item_title)
    public android.widget.TextView title;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_list_item_release_date)
    public android.widget.TextView releaseDate;
    public static final com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieViewHolder.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getRootLayout() {
        return null;
    }
    
    public final void setRootLayout(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getTitle() {
        return null;
    }
    
    public final void setTitle(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getReleaseDate() {
        return null;
    }
    
    public final void setReleaseDate(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    public MovieViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.View itemView) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieViewHolder newInstance(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/view/adapter/MovieViewHolder$Companion;", "", "()V", "newInstance", "Lcom/ramzi/movieviewer/presentation/movielist/view/adapter/MovieViewHolder;", "parent", "Landroid/view/ViewGroup;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieViewHolder newInstance(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}