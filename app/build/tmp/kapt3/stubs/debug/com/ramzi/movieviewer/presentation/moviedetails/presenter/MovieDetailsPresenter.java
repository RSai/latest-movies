package com.ramzi.movieviewer.presentation.moviedetails.presenter;

import java.lang.System;

/**
 * * This is the presenter for Movie Details activity.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001\u000bB\u000f\b\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/ramzi/movieviewer/presentation/moviedetails/presenter/MovieDetailsPresenter;", "Lcom/ramzi/movieviewer/presentation/BasePresenter;", "Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$View;", "Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$Presenter;", "getMovieDetailsUseCase", "Lcom/ramzi/movieviewer/domain/usecase/GetMovieDetailsUseCase;", "(Lcom/ramzi/movieviewer/domain/usecase/GetMovieDetailsUseCase;)V", "fetchMovieDetails", "", "id", "", "MovieDetailsObserver", "app_debug"})
@javax.inject.Singleton()
public final class MovieDetailsPresenter extends com.ramzi.movieviewer.presentation.BasePresenter<com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.View> implements com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.Presenter {
    private final com.ramzi.movieviewer.domain.usecase.GetMovieDetailsUseCase getMovieDetailsUseCase = null;
    
    @java.lang.Override()
    public void fetchMovieDetails(long id) {
    }
    
    @javax.inject.Inject()
    public MovieDetailsPresenter(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.usecase.GetMovieDetailsUseCase getMovieDetailsUseCase) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\b\u0086\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\u0002H\u0016\u00a8\u0006\n"}, d2 = {"Lcom/ramzi/movieviewer/presentation/moviedetails/presenter/MovieDetailsPresenter$MovieDetailsObserver;", "Lio/reactivex/observers/DisposableSingleObserver;", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "(Lcom/ramzi/movieviewer/presentation/moviedetails/presenter/MovieDetailsPresenter;)V", "onError", "", "e", "", "onSuccess", "movieModel", "app_debug"})
    public final class MovieDetailsObserver extends io.reactivex.observers.DisposableSingleObserver<com.ramzi.movieviewer.domain.model.MovieModel> {
        
        @java.lang.Override()
        public void onSuccess(@org.jetbrains.annotations.NotNull()
        com.ramzi.movieviewer.domain.model.MovieModel movieModel) {
        }
        
        @java.lang.Override()
        public void onError(@org.jetbrains.annotations.NotNull()
        java.lang.Throwable e) {
        }
        
        public MovieDetailsObserver() {
            super();
        }
    }
}