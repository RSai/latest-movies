package com.ramzi.movieviewer.domain.usecase.base;

import java.lang.System;

/**
 * * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * * This interface represents a execution unit for different use cases (this means any use case
 * * in the application should implement this contract).
 * *
 * * By convention each UseCase implementation will return the result using a [DisposableSingleObserver]
 * * that will execute its job in a background thread and will post the result in the UI thread.
 * *
 * * This use case is to be used when we expect a single value to be emitted via a [Single].
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0006\b\u0001\u0010\u0002 \u00002\u00020\u0003B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00000\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00018\u0001H&\u00a2\u0006\u0002\u0010\u000eJ\u001d\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\f2\b\u0010\r\u001a\u0004\u0018\u00018\u0001H\u0002\u00a2\u0006\u0002\u0010\u000eJ\'\u0010\u0010\u001a\u00020\u00112\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00000\u00132\n\b\u0002\u0010\r\u001a\u0004\u0018\u00018\u0001\u00a2\u0006\u0002\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/ramzi/movieviewer/domain/usecase/base/SingleUseCase;", "Results", "Params", "Lcom/ramzi/movieviewer/domain/usecase/base/BaseReactiveUseCase;", "threadExecutor", "Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;", "postExecutionThread", "Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;", "rxFactory", "Lcom/ramzi/movieviewer/domain/rx/RxFactory;", "(Lcom/ramzi/movieviewer/domain/executor/ThreadExecutor;Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;Lcom/ramzi/movieviewer/domain/rx/RxFactory;)V", "buildUseCaseSingle", "Lio/reactivex/Single;", "params", "(Ljava/lang/Object;)Lio/reactivex/Single;", "buildUseCaseSingleWithSchedulers", "execute", "", "observer", "Lio/reactivex/observers/DisposableSingleObserver;", "(Lio/reactivex/observers/DisposableSingleObserver;Ljava/lang/Object;)V", "app_debug"})
public abstract class SingleUseCase<Results extends java.lang.Object, Params extends java.lang.Object> extends com.ramzi.movieviewer.domain.usecase.base.BaseReactiveUseCase {
    
    /**
     * * Builds an [Single] which will be used when executing the current [SingleUseCase].
     */
    @org.jetbrains.annotations.NotNull()
    public abstract io.reactivex.Single<Results> buildUseCaseSingle(@org.jetbrains.annotations.Nullable()
    Params params);
    
    /**
     * * Executes the current use case.
     *     *
     *     * @param observer [DisposableSingleObserver] which will be listening to the observer build
     *     * by [buildUseCaseSingle] method.
     *     * @param params Parameters (Optional) used to build/execute this use case.
     */
    public final void execute(@org.jetbrains.annotations.NotNull()
    io.reactivex.observers.DisposableSingleObserver<Results> observer, @org.jetbrains.annotations.Nullable()
    Params params) {
    }
    
    /**
     * * Builds a [Single] which will be used when executing the current [SingleUseCase].
     *     * With provided Schedulers
     */
    private final io.reactivex.Single<Results> buildUseCaseSingleWithSchedulers(Params params) {
        return null;
    }
    
    public SingleUseCase(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.executor.ThreadExecutor threadExecutor, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.executor.PostExecutionThread postExecutionThread, @org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.rx.RxFactory rxFactory) {
        super(null, null, null);
    }
}