package com.ramzi.movieviewer.presentation.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007\u00a8\u0006\u000b"}, d2 = {"Lcom/ramzi/movieviewer/presentation/di/module/PresenterModule;", "", "()V", "provideMovieDetailsPresenter", "Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$Presenter;", "getMovieDetailsUseCase", "Lcom/ramzi/movieviewer/domain/usecase/GetMovieDetailsUseCase;", "provideMovieListPresenter", "Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$Presenter;", "getMovieListUseCase", "Lcom/ramzi/movieviewer/domain/usecase/GetMovieListUseCase;", "app_debug"})
@dagger.Module()
public final class PresenterModule {
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final com.ramzi.movieviewer.presentation.movielist.MovieListContract.Presenter provideMovieListPresenter(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.usecase.GetMovieListUseCase getMovieListUseCase) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.Presenter provideMovieDetailsPresenter(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.domain.usecase.GetMovieDetailsUseCase getMovieDetailsUseCase) {
        return null;
    }
    
    public PresenterModule() {
        super();
    }
}