package com.ramzi.movieviewer.data.executor;

import java.lang.System;

/**
 * * Creates [Executor]s
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/ramzi/movieviewer/data/executor/ExecutorFactory;", "", "()V", "createFixedThreadPoolExecutor", "Ljava/util/concurrent/Executor;", "pool", "", "app_debug"})
@javax.inject.Singleton()
public final class ExecutorFactory {
    
    /**
     * * @see Executors.newFixedThreadPool
     */
    @org.jetbrains.annotations.NotNull()
    public final java.util.concurrent.Executor createFixedThreadPoolExecutor(int pool) {
        return null;
    }
    
    @javax.inject.Inject()
    public ExecutorFactory() {
        super();
    }
}