package com.ramzi.movieviewer.presentation;

import java.lang.System;

/**
 * * This class provides main UI thread for {@link com.ramzi.imdbviewer.domain.usecase.base.BaseReactiveUseCase}
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"}, d2 = {"Lcom/ramzi/movieviewer/presentation/UIThread;", "Lcom/ramzi/movieviewer/domain/executor/PostExecutionThread;", "()V", "getScheduler", "Lio/reactivex/Scheduler;", "app_debug"})
@javax.inject.Singleton()
public final class UIThread implements com.ramzi.movieviewer.domain.executor.PostExecutionThread {
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Scheduler getScheduler() {
        return null;
    }
    
    public UIThread() {
        super();
    }
}