package com.ramzi.movieviewer.presentation.movielist.view;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0001HB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0016\u00106\u001a\u0002072\f\u00108\u001a\b\u0012\u0004\u0012\u00020:09H\u0016J\b\u0010;\u001a\u000207H\u0016J\b\u0010<\u001a\u000207H\u0002J\u0012\u0010=\u001a\u0002072\b\u0010>\u001a\u0004\u0018\u00010?H\u0016J\u0012\u0010@\u001a\u0002072\b\u0010A\u001a\u0004\u0018\u00010BH\u0014J\b\u0010C\u001a\u000207H\u0014J\u0012\u0010D\u001a\u0002072\b\b\u0001\u0010E\u001a\u00020FH\u0016J\b\u0010G\u001a\u000207H\u0016R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0007\"\u0004\b\b\u0010\tR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001e\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u00020\u00198FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001a\u0010\u001bR\u001e\u0010\u001e\u001a\u00020\u001f8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001e\u0010$\u001a\u00020%8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u001e\u0010*\u001a\u00020+8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u001e\u00100\u001a\u0002018\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u00103\"\u0004\b4\u00105\u00a8\u0006I"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/view/MovieListActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$View;", "Landroid/view/View$OnClickListener;", "()V", "isLoading", "", "()Z", "setLoading", "(Z)V", "linearLayoutManager", "Landroid/support/v7/widget/LinearLayoutManager;", "getLinearLayoutManager", "()Landroid/support/v7/widget/LinearLayoutManager;", "movieListAdapter", "Lcom/ramzi/movieviewer/presentation/movielist/view/adapter/MovieListAdapter;", "getMovieListAdapter", "()Lcom/ramzi/movieviewer/presentation/movielist/view/adapter/MovieListAdapter;", "movieListNavigator", "Lcom/ramzi/movieviewer/presentation/movielist/navigator/MovieListNavigator;", "getMovieListNavigator", "()Lcom/ramzi/movieviewer/presentation/movielist/navigator/MovieListNavigator;", "setMovieListNavigator", "(Lcom/ramzi/movieviewer/presentation/movielist/navigator/MovieListNavigator;)V", "presentationComponent", "Lcom/ramzi/movieviewer/presentation/di/component/PresentationComponent;", "getPresentationComponent", "()Lcom/ramzi/movieviewer/presentation/di/component/PresentationComponent;", "presentationComponent$delegate", "Lkotlin/Lazy;", "presenter", "Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$Presenter;", "getPresenter", "()Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$Presenter;", "setPresenter", "(Lcom/ramzi/movieviewer/presentation/movielist/MovieListContract$Presenter;)V", "progressBar", "Landroid/widget/ProgressBar;", "getProgressBar", "()Landroid/widget/ProgressBar;", "setProgressBar", "(Landroid/widget/ProgressBar;)V", "recyclerView", "Landroid/support/v7/widget/RecyclerView;", "getRecyclerView", "()Landroid/support/v7/widget/RecyclerView;", "setRecyclerView", "(Landroid/support/v7/widget/RecyclerView;)V", "rootLayout", "Landroid/widget/FrameLayout;", "getRootLayout", "()Landroid/widget/FrameLayout;", "setRootLayout", "(Landroid/widget/FrameLayout;)V", "addToMovieList", "", "movieList", "", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "hideProgressBar", "initialize", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "showError", "errorMessage", "", "showProgressBar", "RecyclerViewScrollListener", "app_debug"})
public final class MovieListActivity extends android.support.v7.app.AppCompatActivity implements com.ramzi.movieviewer.presentation.movielist.MovieListContract.View, android.view.View.OnClickListener {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.ramzi.movieviewer.presentation.movielist.MovieListContract.Presenter presenter;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.ramzi.movieviewer.presentation.movielist.navigator.MovieListNavigator movieListNavigator;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_list_activity_root_layout)
    public android.widget.FrameLayout rootLayout;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_list_activity_recycler_view)
    public android.support.v7.widget.RecyclerView recyclerView;
    @org.jetbrains.annotations.NotNull()
    @butterknife.BindView(value = com.ramzi.movieviewer.R.id.movie_list_activity_progress_bar)
    public android.widget.ProgressBar progressBar;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy presentationComponent$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieListAdapter movieListAdapter = null;
    @org.jetbrains.annotations.NotNull()
    private final android.support.v7.widget.LinearLayoutManager linearLayoutManager = null;
    private boolean isLoading;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.presentation.movielist.MovieListContract.Presenter getPresenter() {
        return null;
    }
    
    public final void setPresenter(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.presentation.movielist.MovieListContract.Presenter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.presentation.movielist.navigator.MovieListNavigator getMovieListNavigator() {
        return null;
    }
    
    public final void setMovieListNavigator(@org.jetbrains.annotations.NotNull()
    com.ramzi.movieviewer.presentation.movielist.navigator.MovieListNavigator p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.FrameLayout getRootLayout() {
        return null;
    }
    
    public final void setRootLayout(@org.jetbrains.annotations.NotNull()
    android.widget.FrameLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.support.v7.widget.RecyclerView getRecyclerView() {
        return null;
    }
    
    public final void setRecyclerView(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.ProgressBar getProgressBar() {
        return null;
    }
    
    public final void setProgressBar(@org.jetbrains.annotations.NotNull()
    android.widget.ProgressBar p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.presentation.di.component.PresentationComponent getPresentationComponent() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.ramzi.movieviewer.presentation.movielist.view.adapter.MovieListAdapter getMovieListAdapter() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.support.v7.widget.LinearLayoutManager getLinearLayoutManager() {
        return null;
    }
    
    public final boolean isLoading() {
        return false;
    }
    
    public final void setLoading(boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initialize() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    @java.lang.Override()
    public void addToMovieList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.ramzi.movieviewer.domain.model.MovieModel> movieList) {
    }
    
    @java.lang.Override()
    public void showError(@android.support.annotation.StringRes()
    int errorMessage) {
    }
    
    @java.lang.Override()
    public void showProgressBar() {
    }
    
    @java.lang.Override()
    public void hideProgressBar() {
    }
    
    public MovieListActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0016\u00a8\u0006\n"}, d2 = {"Lcom/ramzi/movieviewer/presentation/movielist/view/MovieListActivity$RecyclerViewScrollListener;", "Landroid/support/v7/widget/RecyclerView$OnScrollListener;", "(Lcom/ramzi/movieviewer/presentation/movielist/view/MovieListActivity;)V", "onScrolled", "", "recyclerView", "Landroid/support/v7/widget/RecyclerView;", "dx", "", "dy", "app_debug"})
    public final class RecyclerViewScrollListener extends android.support.v7.widget.RecyclerView.OnScrollListener {
        
        @java.lang.Override()
        public void onScrolled(@org.jetbrains.annotations.Nullable()
        android.support.v7.widget.RecyclerView recyclerView, int dx, int dy) {
        }
        
        public RecyclerViewScrollListener() {
            super();
        }
    }
}