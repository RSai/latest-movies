package com.ramzi.movieviewer.presentation.moviedetails;

import java.lang.System;

/**
 * * Interfaces for Movie list view and presenter.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001:\u0002\u0002\u0003\u00a8\u0006\u0004"}, d2 = {"Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract;", "", "Presenter", "View", "app_debug"})
public abstract interface MovieDetailsContract {
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u0012\u0010\u0004\u001a\u00020\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\'J\b\u0010\n\u001a\u00020\u0003H\'\u00a8\u0006\u000b"}, d2 = {"Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$View;", "Lcom/ramzi/movieviewer/presentation/MvpView;", "hideProgressBar", "", "showError", "errorMessage", "", "showMovieDetails", "movieModel", "Lcom/ramzi/movieviewer/domain/model/MovieModel;", "showProgressBar", "app_debug"})
    public static abstract interface View extends com.ramzi.movieviewer.presentation.MvpView {
        
        @android.support.annotation.UiThread()
        public abstract void showMovieDetails(@org.jetbrains.annotations.NotNull()
        com.ramzi.movieviewer.domain.model.MovieModel movieModel);
        
        @android.support.annotation.UiThread()
        public abstract void showError(@android.support.annotation.StringRes()
        int errorMessage);
        
        @android.support.annotation.UiThread()
        public abstract void showProgressBar();
        
        @android.support.annotation.UiThread()
        public abstract void hideProgressBar();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0007"}, d2 = {"Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$Presenter;", "Lcom/ramzi/movieviewer/presentation/PresenterInterface;", "Lcom/ramzi/movieviewer/presentation/moviedetails/MovieDetailsContract$View;", "fetchMovieDetails", "", "id", "", "app_debug"})
    public static abstract interface Presenter extends com.ramzi.movieviewer.presentation.PresenterInterface<com.ramzi.movieviewer.presentation.moviedetails.MovieDetailsContract.View> {
        
        public abstract void fetchMovieDetails(long id);
    }
}