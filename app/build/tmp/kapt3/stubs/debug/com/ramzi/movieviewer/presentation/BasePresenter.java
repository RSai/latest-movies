package com.ramzi.movieviewer.presentation;

import java.lang.System;

/**
 * * Base class that implements the Presenter interface and provides a base implementation for
 * * attachView() and detachView(). It also handles keeping a reference to the mvpView that
 * * can be accessed from the children classes by calling getMvpView().
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0016\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0015\u0010\f\u001a\u00020\r2\u0006\u0010\u0006\u001a\u00028\u0000H\u0017\u00a2\u0006\u0002\u0010\nJ\b\u0010\u000e\u001a\u00020\rH\u0017R*\u0010\u0006\u001a\u0004\u0018\u00018\u00002\b\u0010\u0005\u001a\u0004\u0018\u00018\u0000@BX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u000f"}, d2 = {"Lcom/ramzi/movieviewer/presentation/BasePresenter;", "VIEW", "Lcom/ramzi/movieviewer/presentation/MvpView;", "Lcom/ramzi/movieviewer/presentation/PresenterInterface;", "()V", "<set-?>", "mvpView", "getMvpView", "()Lcom/ramzi/movieviewer/presentation/MvpView;", "setMvpView", "(Lcom/ramzi/movieviewer/presentation/MvpView;)V", "Lcom/ramzi/movieviewer/presentation/MvpView;", "attachView", "", "detachView", "app_debug"})
public class BasePresenter<VIEW extends com.ramzi.movieviewer.presentation.MvpView> implements com.ramzi.movieviewer.presentation.PresenterInterface<VIEW> {
    @org.jetbrains.annotations.Nullable()
    private VIEW mvpView;
    
    @org.jetbrains.annotations.Nullable()
    public final VIEW getMvpView() {
        return null;
    }
    
    private final void setMvpView(VIEW p0) {
    }
    
    @android.support.annotation.CallSuper()
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    VIEW mvpView) {
    }
    
    @android.support.annotation.CallSuper()
    @java.lang.Override()
    public void detachView() {
    }
    
    public BasePresenter() {
        super();
    }
}