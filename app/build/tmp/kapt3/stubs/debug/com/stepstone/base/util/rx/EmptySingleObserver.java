package com.stepstone.base.util.rx;

import java.lang.System;

/**
 * * This is a default handler for business use cases.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0015\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lcom/stepstone/base/util/rx/EmptySingleObserver;", "T", "Lio/reactivex/observers/DisposableSingleObserver;", "()V", "onError", "", "throwable", "", "onSuccess", "result", "(Ljava/lang/Object;)V", "app_debug"})
public class EmptySingleObserver<T extends java.lang.Object> extends io.reactivex.observers.DisposableSingleObserver<T> {
    
    @java.lang.Override()
    public void onSuccess(T result) {
    }
    
    @java.lang.Override()
    public void onError(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable throwable) {
    }
    
    public EmptySingleObserver() {
        super();
    }
}