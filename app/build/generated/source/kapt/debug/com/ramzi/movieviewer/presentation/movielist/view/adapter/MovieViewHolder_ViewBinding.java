// Generated code from Butter Knife. Do not modify!
package com.ramzi.movieviewer.presentation.movielist.view.adapter;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.ramzi.movieviewer.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class MovieViewHolder_ViewBinding implements Unbinder {
  private MovieViewHolder target;

  @UiThread
  public MovieViewHolder_ViewBinding(MovieViewHolder target, View source) {
    this.target = target;

    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.movie_list_item_root_layout, "field 'rootLayout'", LinearLayout.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.movie_list_item_title, "field 'title'", TextView.class);
    target.releaseDate = Utils.findRequiredViewAsType(source, R.id.movie_list_item_release_date, "field 'releaseDate'", TextView.class);
  }

  @Override
  public void unbind() {
    MovieViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rootLayout = null;
    target.title = null;
    target.releaseDate = null;

    this.target = null;
  }
}
