// Generated code from Butter Knife. Do not modify!
package com.ramzi.movieviewer.presentation.moviedetails.view;

import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.ramzi.movieviewer.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class MovieDetailsActivity_ViewBinding implements Unbinder {
  private MovieDetailsActivity target;

  @UiThread
  public MovieDetailsActivity_ViewBinding(MovieDetailsActivity target, View source) {
    this.target = target;

    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_root_layout, "field 'rootLayout'", ConstraintLayout.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_progress_bar, "field 'progressBar'", ContentLoadingProgressBar.class);
    target.titleTextView = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_title, "field 'titleTextView'", TextView.class);
    target.tagLineTextView = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_tag_line, "field 'tagLineTextView'", TextView.class);
    target.posterImageView = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_poster, "field 'posterImageView'", ImageView.class);
    target.genresTextView = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_genres, "field 'genresTextView'", TextView.class);
    target.releaseDateTextView = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_release_date, "field 'releaseDateTextView'", TextView.class);
    target.overviewTextView = Utils.findRequiredViewAsType(source, R.id.movie_details_activity_overview, "field 'overviewTextView'", TextView.class);
  }

  @Override
  public void unbind() {
    MovieDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rootLayout = null;
    target.progressBar = null;
    target.titleTextView = null;
    target.tagLineTextView = null;
    target.posterImageView = null;
    target.genresTextView = null;
    target.releaseDateTextView = null;
    target.overviewTextView = null;

    this.target = null;
  }
}
