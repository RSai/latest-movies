// Generated code from Butter Knife. Do not modify!
package com.ramzi.movieviewer.presentation.movielist.view;

import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.ramzi.movieviewer.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class MovieListActivity_ViewBinding implements Unbinder {
  private MovieListActivity target;

  @UiThread
  public MovieListActivity_ViewBinding(MovieListActivity target, View source) {
    this.target = target;

    target.rootLayout = Utils.findRequiredViewAsType(source, R.id.movie_list_activity_root_layout, "field 'rootLayout'", FrameLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.movie_list_activity_recycler_view, "field 'recyclerView'", RecyclerView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.movie_list_activity_progress_bar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  public void unbind() {
    MovieListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.rootLayout = null;
    target.recyclerView = null;
    target.progressBar = null;

    this.target = null;
  }
}
