# Movie Viewer
An app for fetching and displaying latest movies

---

## What was done

1. Basic functionality to fetch and display recent movies list
2. Showing movie details
3. Possibility to load more pages while browsing the movie list
4. Added tests

---

## About the architecture

This app was built using Uncle Bob's **clean architecture principals**
https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html

The presentation layer uses the **MVP pattern**

---

## To do

Due to time constraints the following items were not done

1. Filter by date option
2. Using Android architecture components (e.g. Paging)
3. UI tests
4. Focus on a nicer UI